For Booking a slot:

          127.0.0.1:8080/book/{faculty_name}/{date}/{time}

For Deleting a slot:

          127.0.0.1:8080/book/{faculty_name}/{date}/{time}

For Holding a meeting:

          127.0.0.1:8080/collective/{faculty_name}/{date}/{time}/{list_of_participants}/{title_of_the_meeting}

For Retrieving a time table:

          127.0.0.1:8080/retrieve/{faculty_name}/{date}

For listing all the meetings in the coming month:

          127.0.0.1:8080/meetings/F10

Some Notes:

{faculty_name} should be from {F1,F2...F10}

{date} should be in yyyy-mm-dd format

{time} should be in 24 hour format

{list_of_participants} should be comma seperated faculty name

          Eg. /F2,F3,F4/