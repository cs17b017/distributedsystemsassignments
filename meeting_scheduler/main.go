package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/mux"
)

// This structure contains the details of all the meetings to be held in a day for a professor
type timetable struct {
	booked        [24]bool
	meeting_title [24]string
}

// This structure contains the details of all the meetings in a day
// 	booking[i] is used to show whether any meeting is scheduled at i-th hour of the day
//	meeting_title[i] is title of the meeting scheduled at i-th hour
//	participants contain the name of all the professors who are going to attend the meeting that is scheduled at i-th hour
type day struct {
	booked        [24]bool
	meeting_title [24]string
	participants  [24][]string
}

// This is to prevent race condition
// 	Mutex is used here
// 	var professor is mapping to professor to date, further date to timetable
//	var common contains
var calender = struct {
	sync.RWMutex
	professor map[string]map[time.Time]*timetable
	common    map[time.Time]*day
}{professor: make(map[string]map[time.Time]*timetable), common: make(map[time.Time]*day)}

// To check if there is any professor with faculty_name exists
func facultyNameCheck(faculty_name string) (string, error) {
	for i := 1; i < 11; i++ {
		name := "F" + strconv.Itoa(i)
		if faculty_name == name {
			return faculty_name, nil
		}
	}
	return faculty_name, errors.New("Wrong Faculty Name")
}

// constructor
func initialize() {
	for i := 1; i < 11; i++ {
		name := "F" + strconv.Itoa(i)
		calender.professor[name] = make(map[time.Time]*timetable)
	}
}

// constructor
func initialize_for_day(date time.Time) {
	if _, exists := calender.common[date]; exists == false {
		calender.common[date] = new(day)
	}
}

// constructor
func initialize_for_faculty(faculty []string, date time.Time) {
	for _, name := range faculty {
		if _, exists := calender.professor[name][date]; exists == false {
			calender.professor[name][date] = new(timetable)
		}
	}
}

// convert date(string format) to date(time.Time format)
func convert(date string) (time.Time, error) {
	if _, err := strconv.Atoi(strings.Split(date, "-")[0]); err != nil {
		return time.Now(), errors.New("Wrong Date")
	}
	year, _ := strconv.Atoi(strings.Split(date, "-")[0])
	if _, err := strconv.Atoi(strings.Split(date, "-")[1]); err != nil {
		return time.Now(), errors.New("Wrong Date")
	}
	month, _ := strconv.Atoi(strings.Split(date, "-")[1])
	if _, err := strconv.Atoi(strings.Split(date, "-")[2]); err != nil {
		return time.Now(), errors.New("Wrong Date")
	}
	day, _ := strconv.Atoi(strings.Split(date, "-")[2])
	d := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
	return d, nil
}

// check if meeting slot is available on particular date and time
func checkDay(faculty []string, date time.Time, time int, flag bool) error {
	if calender.common[date].booked[time] == flag {
		return errors.New("Slot")
	}
	if flag == false {
		if calender.professor[faculty[0]][date].booked[time] == false {
			return errors.New("Already deleted")
		}
	}
	return nil
}

// changing a meeting slot for a faculty
func changeSlot(faculty []string, date time.Time, time int, meeting_title string, flag bool) {
	for _, name := range faculty {
		calender.professor[name][date].booked[time] = flag
		calender.professor[name][date].meeting_title[time] = meeting_title
		if flag == true {
			calender.common[date].booked[time] = true
			calender.common[date].meeting_title[time] = meeting_title
			calender.common[date].participants[time] = append(calender.common[date].participants[time], name)
		} else {
			var l []string = make([]string, 0)
			for _, i := range calender.common[date].participants[time] {
				if i != name {
					l = append(l, i)
				}
			}
			calender.common[date].participants[time] = l
			if len(l) == 0 {
				calender.common[date].meeting_title[time] = ""
				calender.common[date].booked[time] = false
			}

		}
	}
}

// requesting a meeting slot for the professor
func RequestSlot(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	if _, err := facultyNameCheck(vars["faculty"]); err != nil {
		fmt.Fprintf(w, "wrong faculty")
	}
	faculty, _ := facultyNameCheck(vars["faculty"])
	date := vars["date"]
	if _, err := convert(date); err != nil {
		fmt.Fprintf(w, "wrong date")
	} else {
		date, _ := convert(date)
		time, _ := strconv.Atoi(vars["time"])
		time = time / 100
		calender.Lock()
		initialize_for_day(date)
		initialize_for_faculty([]string{faculty}, date)
		if err := checkDay([]string{faculty}, date, time, true); err != nil {
			fmt.Fprintf(w, "Slot already booked\n Request was not completed")
		} else {
			changeSlot([]string{faculty}, date, time, "Normal Meeting", true)
			fmt.Fprintf(w, "Request Completed")
		}
		calender.Unlock()
	}
}

// deleting a meeting slot for the professor
func DeleteSlot(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	if _, err := facultyNameCheck(vars["faculty"]); err != nil {
		fmt.Fprintf(w, "wrong faculty")
	}
	faculty, _ := facultyNameCheck(vars["faculty"])
	date := vars["date"]
	if _, err := convert(date); err != nil {
		fmt.Fprintf(w, "wrong date")
	} else {
		date, _ := convert(date)
		time, _ := strconv.Atoi(vars["time"])
		time = time / 100
		calender.Lock()
		initialize_for_day(date)
		initialize_for_faculty([]string{faculty}, date)
		if err := checkDay([]string{faculty}, date, time, false); err != nil {
			fmt.Fprintf(w, "Slot already empty\n")
		} else {
			changeSlot([]string{faculty}, date, time, "", false)
			fmt.Fprintf(w, "Request Completed")
		}
		calender.Unlock()
	}
}

// request a collective meeting
func RequestMeeting(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	if _, err := facultyNameCheck(vars["faculty"]); err != nil {
		fmt.Fprintf(w, "wrong faculty")
	}
	faculty, _ := facultyNameCheck(vars["faculty"])
	date := vars["date"]
	if _, err := convert(date); err != nil {
		fmt.Fprintf(w, "wrong date")
	} else {
		date, _ := convert(date)
		time, _ := strconv.Atoi(vars["time"])
		time = time / 100
		list_of_professors := append(strings.Split(vars["list"], ","), faculty)
		title := vars["title"]

		calender.Lock()
		initialize_for_day(date)
		initialize_for_faculty(list_of_professors, date)
		if err := checkDay(list_of_professors, date, time, true); err != nil {
			fmt.Fprintf(w, "Slot already booked\n Request was not completed")
		} else {
			changeSlot(list_of_professors, date, time, title, true)
			fmt.Fprintf(w, "Request Completed")
		}
	}
	calender.Unlock()
}

// requesting list of all meetings for a faculty on the given date
func RequestSchedule(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	if _, err := facultyNameCheck(vars["faculty"]); err != nil {
		fmt.Fprintf(w, "wrong faculty")
	}
	faculty, _ := facultyNameCheck(vars["faculty"])
	date := vars["date"]
	if _, err := convert(date); err != nil {
		fmt.Fprintf(w, "wrong date")
	} else {
		date, _ := convert(date)
		initialize_for_faculty([]string{faculty}, date)
		for i := 0; i < 24; i++ {
			if calender.professor[faculty][date].booked[i] == true {
				d := time.Date(date.Year(), date.Month(), date.Day(), i, 0, 0, 0, time.UTC)
				fmt.Fprintf(w, d.Format("2006-01-02 15:04:05")+" \nMeeting Title: "+calender.professor[faculty][date].meeting_title[i]+"\n\n")
			}
		}
		fmt.Fprintf(w, "-----------------------")
	}
}

// requesting a list of all the meetings
func getAllMeetings(w http.ResponseWriter, r *http.Request) {
	for date, day := range calender.common {
		if date.Sub(time.Now()).Hours()/24 > 30 || date.Sub(time.Now()).Hours()/24 <= 0 {
			continue
		}
		for i := 0; i < 24; i++ {
			if day.booked[i] == true {
				d := time.Date(date.Year(), date.Month(), date.Day(), i, 0, 0, 0, time.UTC)
				fmt.Fprintf(w, d.Format("2006-01-02 15:04:05")+" \nMeeting Title: "+day.meeting_title[i]+" \nParticipants:"+strings.Join(day.participants[i], ",")+"\n\n")
			}
		}
		fmt.Fprintf(w, "-----------------------")
	}
}

// mapping urls to functions
func handleRequests() {
	mysite := mux.NewRouter().StrictSlash(true)
	mysite.HandleFunc("/book/{faculty}/{date}/{time}", RequestSlot).Methods("GET")
	mysite.HandleFunc("/delete/{faculty}/{date}/{time}", DeleteSlot).Methods("GET")
	mysite.HandleFunc("/collective/{faculty}/{date}/{time}/{list}/{title}", RequestMeeting).Methods("GET")
	mysite.HandleFunc("/retrieve/{faculty}/{date}", RequestSchedule).Methods("GET")
	mysite.HandleFunc("/meetings/F10", getAllMeetings).Methods("GET")
	log.Fatal(http.ListenAndServe(":8080", mysite))
}
func main() {
	initialize()
	handleRequests()
}
