package main

import (
	"fmt"
	"time"
)

var (
	name string = "manish"
)

func main() {
	// Flags
	// %v prints value
	// %T prints type of the value

	// Complex number
	// declare it complex(2,3)=2+3i
	// real(n) and imag(n) gives real and imaginary number

	// getting ascii code of each character in string
	// []byte(s)

	// append(arr,arr)

	// slice
	// make([]int,length,capacity)

	// map: map[key]value{}
	// delete(map,key)

	// switch: fallthrough

	// fmt.Errorf()

	// sync.WaitGroup
	// .Add
	// sync.RWMutex
	// .RLock .RUnlock

	// runtime.GOMAXPROCS(-1)
	// date := time.Date(2021, 3, 2, 0, 0, 0, 0, time.UTC)
	date := time.Parse("2020-01-23", "2020-01-11")
	fmt.Println(date.Month())

	fmt.Println(date)
}
