Read Google's MapReduce:
https://static.googleusercontent.com/media/research.google.com/en//archive/mapreduce-osdi04.pdf
Write a two to three page report on the same. Avoid copying sentences from the paper.

Write a report on how you would implement MapReduce using Go.
A high level design document of workers, masters, and their interaction etc. A worker might not respond, in such a situtation describe how the master will re assign the work to a different thread.

Read about Go channels here:
https://opensource.com/article/18/7/locks-versus-channels-concurrent-go

```cpp
package main

import "sync"

var ledger int = 0
var m sync.Mutex

func worker1(){
 	m.Lock()
 	ledger = 1
 	m.Unlock()
}

func worker2(){
 	m.Lock()
 	ledger = 2
 	m.Unlock()
}

func worker3(){
	m.Lock()
	ledger = 3
	m.Unlock()
}

func main(){
   go worker1()
   go worker2()
   go worker3()
}
```

Write an equivalent Go code without using locks.
