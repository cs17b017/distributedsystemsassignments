package main

import (
	"fmt"
	"sync"
)

var ledger int = 0
var lock = make(chan int, 1)
var done sync.WaitGroup // for stopping main to not exit early

func worker1() {
	var a int = <-lock
	ledger = 1
	lock <- a
	done.Done()
}
func worker2() {
	var a int = <-lock
	ledger = 2
	lock <- a
	done.Done()
}
func worker3() {
	var a int = <-lock
	ledger = 3
	lock <- a
	done.Done()
}
func main() {
	lock <- 1   //initialize the channel
	done.Add(3) //this is just to check the value of ledger at the end
	// otherwise the main is ending early
	go worker1()
	go worker2()
	go worker3()
	done.Wait()
	fmt.Println(ledger)
}
