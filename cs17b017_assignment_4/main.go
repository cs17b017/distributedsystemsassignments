package main

import (
	"fmt"
	"os"

	"github.com/PuerkitoBio/goquery"
	"github.com/gocolly/colly"
)

func main() {
	searchString := os.Args[1]
	c := colly.NewCollector(
		colly.AllowedDomains("flipkart.com/search?q=" + searchString),
	)
	c.OnHTML("div", func(e *colly.HTMLElement) {
		allDiv := e.DOM.ParentsUntil("~").Find("div")
		allDiv.Each(func(i int, s *goquery.Selection) {
			class_name, _ := s.Attr("class")
			if class_name == "_4ddWXP" {
				item := s.Text
				fmt.Println(item)
			}
		})
	})
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		c.Visit(e.Request.AbsoluteURL(link))
	})
	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.Visit("https://www.flipkart.com/search?q=" + searchString)
}
