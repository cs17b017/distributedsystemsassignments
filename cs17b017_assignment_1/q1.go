package main

import "fmt"

type Tree struct {
	left  *Tree
	right *Tree
	value int
}

func (T *Tree) insert(a int) {
	if T.value > a {
		if T.left != nil {
			T.left.insert(a)
		} else {
			node := Tree{
				left:  nil,
				right: nil,
				value: a,
			}
			T.left = &node
		}
	} else {
		if T.right != nil {
			T.right.insert(a)
		} else {
			node := Tree{
				left:  nil,
				right: nil,
				value: a,
			}
			T.right = &node
		}
	}
}
func (T *Tree) inorder() {
	if T.left != nil {
		T.left.inorder()
	}
	fmt.Print(T.value, " ")
	if T.right != nil {
		T.right.inorder()
	}
}
func main() {
	var n int
	fmt.Scanln(&n)
	head := &Tree{
		left:  nil,
		right: nil,
		value: 0,
	}
	initialFlag := true
	for i := 0; i < n; i++ {
		var a int
		fmt.Scanln(&a)
		if initialFlag {
			head.value = a
			initialFlag = false
		} else {
			head.insert(a)
		}

	}
	head.inorder()
	fmt.Println()
}
