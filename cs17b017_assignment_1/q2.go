package main

import "fmt"

type P struct {
	a int
	b int
}

var ans = make([]P, 0, 10)
var arr = make([][]int, 3)
var size int = 4

func queens(n int) {
	if n == size {
		fmt.Println(ans)
		return
	}
	var visited [8]bool
	for a := range visited {
		visited[a] = true
	}
	for a := range arr[0] {
		visited[arr[0][a]] = false
	}
	for a := range arr[1] {
		arr[1][a] = arr[1][a] - 1
		if arr[1][a] >= 0 && arr[1][a] < 8 {
			visited[arr[1][a]] = false
		}
	}
	for a := range arr[2] {
		arr[2][a] = arr[2][a] + 1
		if arr[2][a] >= 0 && arr[2][a] < 8 {
			visited[arr[2][a]] = false
		}
	}
	for a, b := range visited {
		if b == true && a < size {
			arr[0] = append(arr[0], a)
			arr[1] = append(arr[1], a)
			arr[2] = append(arr[2], a)
			ans = append(ans, P{n, a})
			queens(n + 1)
			arr[0] = arr[0][:len(arr[0])-1]
			arr[1] = arr[1][:len(arr[1])-1]
			arr[2] = arr[2][:len(arr[2])-1]
			ans = ans[:len(ans)-1]
		}
	}

}
func main() {
	arr[0] = make([]int, 0, 10)
	arr[1] = make([]int, 0, 10)
	arr[2] = make([]int, 0, 10)
	queens(0)

}
