package main

import (
	"fmt"
	"sync"
)

// Semaphore structure
//-------------------------------------------------------------
type semaphore struct {
	arr chan int
}

func (S semaphore) signal(a int) {
	for i := 0; i < a; i++ {
		S.arr <- 0
	}
}
func (S semaphore) wait() {
	_ = <-S.arr
}

//-------------------------------------------------------------
var s = semaphore{arr: make(chan int, 100)}

// max limit of semaphore 100
//-------------------------------------------------------------
var done sync.WaitGroup // for stopping main to not exit early

var value = 10

func test() {
	s.wait() // semaphore wait
	value--
	fmt.Println(value)
	s.signal(1)
	done.Done()
}
func main() {
	s.signal(3) // semaphore signal
	for i := 0; i < 10; i++ {
		done.Add(1)
		go test()
	}
	done.Wait()
}
